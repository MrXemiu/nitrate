﻿# Nitrate main script

# stop on errors
$ErrorActionPreference='Stop'

# Initialize root paths
$scriptPath = Split-Path -parent $MyInvocation.MyCommand.Definition

Import-Module -Force -Name "$scriptPath\Console"

$rootPath = ""
Push-Location
do
{
	if (Test-Path ".\NitrateConfig.ps1")
	{
		$rootPath = (Get-Location -PSProvider FileSystem).ProviderPath
	}
	else
	{
		cd ..
	}
}
while (($rootPath -eq "") -and -not((Get-Location -PSProvider FileSystem).ProviderPath.length -eq 3))
Pop-Location

if ($args[0] -eq "init")
{
	if (-not($rootPath -eq ""))
	{
		Write-Host
		CON_WriteInfo "A configuration file was found at [$rootPath], you may not initialize a new environment." $true
		Write-Host
		exit
	}
	
	copy "$scriptPath\DefaultData.ps1" "NitrateConfig.ps1"
	
	Write-Host
	CON_WriteInfo "Your environment is initialized." $true
	Write-Host "You should edit NitrateConfig.ps1 to configure the settings."
	Write-Host "Then setup Orchard by calling Nitrate.ps1 setup"
	Write-Host
	exit
}
elseif ($rootPath -eq "")
{
	Write-Host
	CON_WriteInfo "Configuration file NitrateConfig.ps1 could not be found." $true
	Write-Host "Use Nitrate.ps1 init to initialize a new environment"
	Write-Host
	exit
}

# load all additional modules, -Force ensures they get reloaded in case they get changed
Import-Module -Force -Name "$scriptPath\FileSystem"
Import-Module -Force -Name "$scriptPath\Net"
Import-Module -Force -Name "$scriptPath\IIS"
Import-Module -Force -Name "$scriptPath\SQL"

# load the default and custom data
. "$scriptPath\DefaultData.ps1"
. "$rootPath\NitrateConfig.ps1"
if (Test-Path "$rootPath\NitrateLocal.ps1")
{
	. "$rootPath\NitrateLocal.ps1"
}

# initialize some values
$resetPasswordFile = "$scriptPath\ResetAdmin.sql"

# Ensure source folders exist
FS_EnsureDir("$rootPath\db")
FS_EnsureDir("$rootPath\source")
FS_EnsureDir("$rootPath\source\modules")
FS_EnsureDir("$rootPath\source\themes")
FS_EnsureDir("$rootPath\source\media")

# utility functions
function UnZip($source, $destination)
{
	CON_WriteInfo "Unzipping $source... "
	$pathExists = Test-Path "$destination"
	if ($pathExists -eq $false){New-Item -ItemType directory -Path "$destination"}
	. "$scriptPath\tools\unzip.exe" -q "$source" -d $destination
	CON_WriteDone
}

# Removes all of the symlinks
function CleanLinks
{
	FS_UnlinkFolders "$rootPath\source\themes" "$rootPath\orchard\src\orchard.web\themes"
	FS_UnlinkFolders "$rootPath\source\modules" "$rootPath\orchard\src\orchard.web\modules"
	FS_UnlinkFolder "$rootPath\orchard\src\orchard.web\Media"	
	if ($DAT_CopySolution -eq $true) {
		FS_UnlinkFile "$rootPath\orchard\src\Orchard.sln"
	}
}

# Creates all of the symlinks
function CreateLinks
{
	if ($DAT_CopySolution -eq $true) {
		FS_LinkFile "$rootPath\source\Orchard.sln" "$rootPath\orchard\src\Orchard.sln"
	}
	FS_LinkFolders "$rootPath\source\themes" "$rootPath\orchard\src\orchard.web\themes"
	FS_LinkFolders "$rootPath\source\modules" "$rootPath\orchard\src\orchard.web\modules"
	FS_LinkFolder "$rootPath\source\media\" "$rootPath\orchard\src\orchard.web\Media"
}

###########################################################################################################
#
#  cleans existing environment
#
function Clean
{
	CON_WriteInfo "Cleaning up the site..." $true

	if ($DAT_useIIS -eq $true)
	{
		. touch "$rootPath\orchard\src\orchard.web\web.config"
		if (-not($DAT_webAppName -eq ""))
		{		
			IIS_RemoveApplication $DAT_websiteName $DAT_webAppName
		}
		elseif ($DAT_canDeleteWebside)
		{
			IIS_RemoveSite $DAT_websiteName
		}
	}
	CleanLinks
	FS_RemoveDir "$rootPath\orchard"
	SQL_DeleteDb $DAT_SqlServer $DAT_SqlInstance $DAT_SqlDatabase
	SQL_DeleteDbUser $DAT_SqlServer $DAT_SqlInstance $DAT_SqlUser

	CON_WriteGood "All done!"
}

###########################################################################################################
#
#  Reinitializes the database
#
function CleanDb ($ext_id, $recipe_name)
{
	sql_deletedb $dat_sqlserver $dat_sqlinstance $dat_sqldatabase
	sql_deletedbuser $dat_sqlserver $dat_sqlinstance $dat_sqluser
	
	sql_createdb $dat_sqlserver $dat_sqlinstance $dat_sqldatabase
	sql_createdbuser $dat_sqlserver $dat_sqlinstance $dat_sqldatabase $dat_sqluser $dat_sqlpassword
	
	Push-Location
	cd "$rootPath\orchard\src\orchard.web\bin\"
	$connectionString = SQL_MakeConnectionString $DAT_SqlServer $DAT_SqlInstance $DAT_SqlDatabase $DAT_SqlUser $DAT_SqlPassword
	Write-Host $connectionString
	ResetSiteAppData	
	. ".\orchard.exe" setup /sitename:$DAT_OrchardSiteName /adminusername:$DAT_OrchardAdminUser /adminpassword:$DAT_OrchardAdminPassword /databaseprovider:SQLServer /DatabaseConnectionString:"$connectionString" /recipe:Default
	. ".\orchard.exe" feature enable Orchard.CodeGeneration
	. ".\orchard.exe" feature enable Orchard.ImportExport
	if (($ext_id) -and ($recipe_name))
	{
		. ".\orchard.exe" recipes execute $ext_id $recipe_name
	}
	Pop-Location
	
	CON_WriteGood "Database clean up complete!"
	Write-Host "User name : $DAT_OrchardAdminUser"
	Write-Host "Password  : $DAT_OrchardAdminPassword"
}

###########################################################################################################
#
#  builds a new environment
#
function Setup
{
	Clean

	CON_WriteInfo "Retrieving Orchard source for branch $DAT_CodeBranch" $true
	. git clone -b $DAT_CodeBranch --single-branch https://git01.codeplex.com/orchard "$rootPath\orchard"
	#. hg clone https://hg.codeplex.com/orchard "$rootPath\orchard" -b $DAT_CodeBranch -r $DAT_CodeTag
	CON_WriteDone
	
	CON_WriteInfo "Building Orchard" $true
	. msbuild "$rootPath\orchard\src\orchard.sln"
	CON_WriteDone

	if ($DAT_CopySolution -eq $true) {
		if (-not(Test-Path "$rootPath\source\Orchard.sln")) {
			mv "$rootPath\orchard\src\Orchard.sln" "$rootPath\source\Orchard.sln"
		}
		else {
			del "$rootPath\orchard\src\Orchard.sln"
		}
	}
	
	Remove-Item "$rootPath\orchard\src\orchard.web\Media" -Force -Recurse

	CreateLinks
	
	SQL_CreateDb $DAT_SqlServer $DAT_SqlInstance $DAT_SqlDatabase
	SQL_CreateDbUser $DAT_SqlServer $DAT_SqlInstance $DAT_SqlDatabase $DAT_SqlUser $DAT_SqlPassword

	CON_WriteInfo "Creating Orchard site... (this takes a while as Orchard downloads additional modules)" $true
	Push-Location
	cd "$rootPath\orchard\src\orchard.web\bin\"
	$connectionString = SQL_MakeConnectionString $DAT_SqlServer $DAT_SqlInstance $DAT_SqlDatabase $DAT_SqlUser $DAT_SqlPassword
	Write-Host $connectionString
	. ".\orchard.exe" setup /sitename:$DAT_OrchardSiteName /adminusername:$DAT_OrchardAdminUser /adminpassword:$DAT_OrchardAdminPassword /databaseprovider:SQLServer /DatabaseConnectionString:"$connectionString" /recipe:Default
	. ".\orchard.exe" feature enable Orchard.CodeGeneration
	. ".\orchard.exe" feature enable Orchard.ImportExport
	if (Test-Path "$rootPath\orchard\src\orchard.web\Modules\Nitrate\Recipes\Nitrate.recipe.xml") {
		. ".\orchard.exe" recipes execute Nitrate Nitrate
	}
	Pop-Location
	CON_WriteDone

	RestoreDb

	if ($DAT_useIIS -eq $true)
	{
		if ($DAT_webAppName -eq "")
		{
			IIS_CreateSite $DAT_websiteName $DAT_protocol $DAT_bindingInfo "$rootPath\orchard\src\orchard.web" $DAT_appPoolName
		}
		else
		{
			IIS_CreateApplication $DAT_websiteName $DAT_webAppName "$rootPath\orchard\src\orchard.web" $DAT_appPoolName
		}
	}
	
	CON_WriteGood "Your development environment is ready!"
	Write-Host "User name : $DAT_OrchardAdminUser"
	Write-Host "Password  : $DAT_OrchardAdminPassword"
}

###########################################################################################################
#
#  backs up the site's database
#
function BackupDb($fileName)
{
	if (!$fileName)
	{
		$fileName = $DAT_SqlFileName
	}

	$backupPath = "$rootPath\db\$fileName.bak"
	$backupArchive = "$rootPath\db\$fileName.zip"

	if (Test-Path $backupPath)
	{
		Remove-Item $backupPath
	}

	if (Test-Path $backupArchive)
	{
		Remove-Item $backupArchive
	}

	SQL_BackupDb $DAT_SqlServer $DAT_SqlInstance $DAT_SqlDatabase $backupPath

	Push-Location
	cd "$rootPath\db"
	. "$scriptPath\Tools\zip.exe" -9 -m "$fileName.zip" "$fileName.bak"
	Pop-Location
}

###########################################################################################################
#
#  restores the site's database
#
function RestoreDb($fileName)
{
	if (!$fileName)
	{
		$fileName = $DAT_SqlFileName
	}

	$backupPath = "$rootPath\db\$fileName.bak"
	$backupArchive = "$rootPath\db\$fileName.zip"

	if (Test-Path $backupArchive)
	{
		UnZip $backupArchive "$rootPath\db\"

		if (Test-Path $backupPath)
		{
			. touch "$rootPath\orchard\src\orchard.web\web.config"
			SQL_DeleteDb $DAT_SqlServer $DAT_SqlInstance $DAT_SqlDatabase
			SQL_RestoreDb $DAT_SqlServer $DAT_SqlInstance $DAT_SqlDatabase $backupPath $DAT_SqlUser $resetPasswordFile
			Remove-Item $backupPath
		}
	}
}

###########################################################################################################
#
#  Synchronizes the orchard site on an ftp server
#
function FtpSync
{
	CON_WriteInfo "Synchronizing site... "
	. "$scriptPath\tools\winscp.exe" /console /script="$scriptPath\winscp.txt" /parameter "$DAT_FtpUrl" "$rootPath\orchard\build\precompiled" "$DAT_FtpRoot" "$rootPath\orchard\src\orchard.web\app_data\Sites\Default\Settings.txt"
	CON_WriteDone
}

###########################################################################################################
#
#  creates a new module, adds it to Orchard and the source folder.
#
function CreateModule($name)
{
	Push-Location
	CON_WriteInfo "Generating module with Orchard... " $true
	cd "$rootPath\orchard\src\orchard.web\bin\"
	. ".\orchard.exe" codegen module $name /IncludeInSolution:true
	Pop-Location
	CON_WriteDone
	CON_WriteInfo "Moving files to source folder... "
	Move-Item "$rootPath\orchard\src\orchard.web\modules\$name" "$rootPath\source\modules\"
	CON_WriteDone
	FS_LinkFolder "$rootPath\source\modules\$name" "$rootPath\orchard\src\orchard.web\modules\$name"
}

###########################################################################################################
#
#  creates a new theme, adds it to Orchard and the source folder.
#
function CreateTheme($name)
{
	Push-Location
	CON_WriteInfo "Generating theme with Orchard... " $true
	cd "$rootPath\orchard\src\orchard.web\bin\"
	. ".\orchard.exe" codegen theme $name /CreateProject:true /IncludeInSolution:true
	Pop-Location
	CON_WriteDone
	CON_WriteInfo "Moving files to source folder... "
	Move-Item "$rootPath\orchard\src\orchard.web\themes\$name" "$rootPath\source\themes\"
	CON_WriteDone
	FS_LinkFolder "$rootPath\source\themes\$name" "$rootPath\orchard\src\orchard.web\themes\$name"
}

###########################################################################################################
#
#  Shell
#
function Shell($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9)
{
	Push-Location
	cd "$rootPath\orchard\src\orchard.web\bin\"
	. ".\orchard.exe" $p1 $p2 $p3 $p4 $p5 $p6 $p7 $p8 $p9
	Pop-Location
}

###########################################################################################################
#
#  Determines which modules and themes have been installed from the gallery
#
function BuildRecipe
{
	CON_WriteInfo "Generating Recipe... " $true

	# Ensure the Nitrate 'Module' exists
	if (-not(Test-Path "$rootPath\orchard\src\orchard.web\Modules\Nitrate")) {
		FS_EnsureDir "$rootPath\source\Modules\Nitrate"
		FS_EnsureDir "$rootPath\source\Modules\Nitrate\Recipes"
		copy "$scriptPath\Module.txt" "$rootPath\source\Modules\Nitrate"
		FS_LinkFolder "$rootPath\source\modules\Nitrate" "$rootPath\orchard\src\orchard.web\modules\Nitrate"
	}

	$stream = [System.IO.StreamWriter] "$rootPath\source\Modules\Nitrate\Recipes\Nitrate.recipe.xml"
    $stream.WriteLine(@'
<?xml version="1.0"?>
<Orchard>
  <Recipe>
    <Name>Nitrate</Name>
    <Description>Auto generated by Nitrate</Description>
    <Author>Nitrate</Author>
    <WebSite>https://bitbucket.org/Ventajou/nitrate</WebSite>
    <Tags></Tags>
    <Version>1.0</Version>
  </Recipe>
'@)

	$modules = FS_GetFolderNames "$rootPath\source\modules"
	$themes = FS_GetFolderNames "$rootPath\source\themes"

	$modulesRegex = '(?m)^.*?/Modules/(.*?)/Module.txt'
	$themesRegex = '(?m)^.*?/Themes/(.*?)/Theme.txt'
	
	Push-Location
	
	cd "$rootPath\orchard"
	$untracked = (git status -u --porcelain | Out-String)

	$m = ($untracked | Select-String -AllMatches $modulesRegex)
	foreach($match in $m.Matches) {
		$moduleName = $match.Groups[1].Value
		if ($modules -notcontains $moduleName) {
			$stream.WriteLine("  <Module packageId=""Orchard.Module.$moduleName"" />")
		}
	}

	$m = ($untracked | Select-String -AllMatches $themesRegex)
	foreach($match in $m.Matches) {
		$themeName = $match.Groups[1].Value
		if ($themes -notcontains $themeName) {
			$stream.WriteLine("  <Theme packageId=""Orchard.Theme.$themeName"" />")
		}
	}

	$stream.WriteLine('</Orchard>')
	$stream.close()
	
	Pop-Location
	CON_WriteDone
}

###########################################################################################################
#
# removes and restores all the symlinks to your source folders and files.
#
function RebuildLinks
{
	CON_WriteInfo "Recreating all symlinks... " $true
	CleanLinks
	CreateLinks
	CON_WriteDone
}

###########################################################################################################
#
# tails the log file.
#
function TailLog
{
	$date = Get-Date -format yyyy.MM.dd
	CON_WriteInfo "Watching logfile: orchard-error-$date.log" $true
	CON_WriteInfo "Press Ctrl+C to stop" $true
	Write-Host
	. "tail" -f "$rootPath\orchard\src\orchard.web\app_data\logs\orchard-error-$date.log"
}

###########################################################################################################
#
#  Deletes the mappings.bin file
#
function KillMappings
{
	CON_WriteInfo "Removing mappings.bin... " $true
	. touch "$rootPath\orchard\src\orchard.web\web.config"
	Remove-Item -Path "$rootPath\orchard\src\orchard.web\app_data\Sites\Default\mappings.bin"
	CON_WriteDone
}

###########################################################################################################
#
#  Deletes the mappings.bin file
#
function ResetSiteAppData
{
	CON_WriteInfo "Removing site app_data... " $true
	. touch "$rootPath\orchard\src\orchard.web\web.config"
	Remove-Item -Path "$rootPath\orchard\src\orchard.web\app_data\Sites\Default\*" -recurse
	CON_WriteDone
}

###########################################################################################################
#
#  Main
#
switch ($args[0])
{
	"clean" {
		Clean
	}
	"clean-db"{
		CleanDb $args[1] $args[2]
	}
	"setup" {
		Setup
	}
	"backup-db" {
		BackupDb $args[1]
	}
	"load-db" {
		RestoreDb $args[1]
	}
	"ftp-sync" {
		FtpSync
	}
	"create-module" {
		if ($args[1] -match "^[\w\.]+$")
		{
			CreateModule $args[1]
		}
		else
		{
			Write-Host
			Write-Host "Invalid module name!"
			Write-Host
			Write-Host "Syntax: Nitrate.ps1 create-module <module_name>"
			Write-Host
		}
	}
	"create-theme" {
		if ($args[1] -match "^[\w\.]+$")
		{
			CreateTheme $args[1]
		}
		else
		{
			Write-Host
			Write-Host "Invalid theme name!"
			Write-Host
			Write-Host "Syntax: Nitrate.ps1 create-theme <theme_name>"
			Write-Host
		}
	}
	"shell" {
		Shell $args[1] $args[2] $args[3] $args[4] $args[5] $args[6] $args[7] $args[8] $args[9]
	}
	"build-recipe" {
		BuildRecipe
	}
	"rebuild-links" {
		RebuildLinks
	}
	"log" {
		TailLog
	}
	"kill-mappings" {
		KillMappings
	}
	default {
		Write-Host
		Write-Host "Usage: Nitrate.ps1 <command>"
		Write-Host
		Write-Host "Available commands:"
		Write-Host " - clean:         cleans existing environment."
		Write-Host " - clean-db:	  reinitializes the site database.  Optionally specify module name and recipe name to execute recipe on clean database."	
		Write-Host " - setup:         builds a new environment."
		Write-Host " - backup-db:     backs up the site's database."
		Write-Host " - load-db:       restores the site's database."
		Write-Host " - ftp-sync:      synchronizes the orchard site on an ftp server."
		Write-Host " - create-module: creates a new module, adds it to Orchard and the source folder."
		Write-Host " - create-theme:  creates a new theme, adds it to Orchard and the source folder."
		Write-Host " - shell:         runs the Orchard command line."
		Write-Host " - build-recipe:  creates a recipe file that contains the list of modules and themes downloaded from the gallery."
		Write-Host " - rebuild-links: removes and restores all the symlinks to your source folders and files."
		Write-Host " - log:			  tails the log file."
		Write-Host " - kill-mappings: deletes the mappings.bin file."
		Write-Host
	}
}
